import math
import os.path
import pytest

from bamboo.root import loadJMESystematicsCalculators, gbl
from CMSJMECalculators import config as calcConfigs

testData = os.path.join(os.path.dirname(__file__), "data")


def toRVecFloat(values):
    from bamboo.root import gbl
    RVec_float = gbl.ROOT.VecOps.RVec["float"]
    res = RVec_float(len(values), 0.)
    for i, val in enumerate(values):
        res[i] = val
    return res


def toRVecInt(values):
    from bamboo.root import gbl
    RVec_int = gbl.ROOT.VecOps.RVec["int"]
    res = RVec_int(len(values), 0)
    for i, val in enumerate(values):
        res[i] = val
    return res


def getEventWith(f, condition=lambda ev: True, treeName="Events"):
    tup = f.Get(treeName)
    tup.GetEntry(0)
    i = 0
    while not condition(tup):
        i += 1
        tup.GetEntry(i)
    yield tup


def getMETUnclDeltaXY(ptNom, phiNom, ptUp, phiUp):
    """
    Calculate MetUnclustEnUpDeltaX and MetUnclustEnUpDeltaY
    from MET_ptUnclusteredUp and MET_phiUnclusteredUp
    """
    p4Nom = gbl.TLorentzVector()
    p4Nom.SetPtEtaPhiE(ptNom, 0., phiNom, 0.)
    p4Up = gbl.TLorentzVector()
    p4Up.SetPtEtaPhiE(ptUp, 0., phiUp, 0.)
    p4 = p4Up - p4Nom
    return p4.Px(), p4.Py()


def getJetMETArgs(tup, isMC=True, forMET=False, PuppiMET=False, addHEM2018Issue=False):
    """ Get the input values for the jet/met variations calculator from a tree (PyROOT-style) """
    args = [
        toRVecFloat(tup.Jet_pt),
        toRVecFloat(tup.Jet_eta),
        toRVecFloat(tup.Jet_phi),
        toRVecFloat(tup.Jet_mass),
        toRVecFloat(tup.Jet_rawFactor),
        toRVecFloat(tup.Jet_area),
    ]
    if forMET:
        args += [
            toRVecFloat(tup.Jet_muonSubtrFactor),
            toRVecFloat(tup.Jet_neEmEF),
            toRVecFloat(tup.Jet_chEmEF),
        ]
    args.append(toRVecInt(tup.Jet_jetId if addHEM2018Issue else []))
    rhoNames = ["fixedGridRhoFastjetAll", "Rho_fixedGridRhoFastjetAll"]
    for rhoName in rhoNames:
        try:
            rho = getattr(tup, rhoName)
            args.append(rho)
            break
        except AttributeError:
            pass
    else:
        raise RuntimeError(f"None of the attributes {rhoNames} found.")
    if isMC:
        args += [
            toRVecInt(tup.Jet_genJetIdx),
            toRVecInt(tup.Jet_partonFlavour),
            tup.event,
            toRVecFloat(tup.GenJet_pt),
            toRVecFloat(tup.GenJet_eta),
            toRVecFloat(tup.GenJet_phi),
            toRVecFloat(tup.GenJet_mass)
        ]
    else:
        args += [toRVecInt([]), toRVecInt([]), 0, toRVecFloat([]), toRVecFloat([]),
                 toRVecFloat([]), toRVecFloat([])]
    if forMET:
        if PuppiMET:
            args += [tup.RawPuppiMET_phi, tup.RawPuppiMET_pt]
        else:
            args += [tup.RawMET_phi, tup.RawMET_pt]
        args += [toRVecFloat(getattr(tup, "CorrT1METJet_{0}".format(varNm)))
                 for varNm in ("rawPt", "eta", "phi", "area", "muonSubtrFactor")]
        args += [toRVecFloat([]), toRVecFloat([])]
        if PuppiMET:
            if (tup.PuppiMET_ptUnclusteredUp is not None
               and tup.PuppiMET_phiUnclusteredUp is not None):
                MetUnclustEnUpDeltaX, MetUnclustEnUpDeltaY = getMETUnclDeltaXY(
                    tup.PuppiMET_pt, tup.PuppiMET_phi,
                    tup.PuppiMET_ptUnclusteredUp, tup.PuppiMET_phiUnclusteredUp)
                args += [MetUnclustEnUpDeltaX, MetUnclustEnUpDeltaY]
            elif (tup.PuppiMET_MetUnclustEnUpDeltaX is not None
                  and tup.PuppiMET_MetUnclustEnUpDeltaY is not None):
                args += [tup.PuppiMET_MetUnclustEnUpDeltaX, tup.PuppiMET_MetUnclustEnUpDeltaY]
            else:
                raise RuntimeError(f"None of the variables related to MET Uncl. energy found.")
        else:
            args += [tup.MET_MetUnclustEnUpDeltaX, tup.MET_MetUnclustEnUpDeltaY]
    return args


def getFatJetArgs(tup, isMC=True, addHEM2018Issue=False):
    args = [
        toRVecFloat(tup.FatJet_pt),
        toRVecFloat(tup.FatJet_eta),
        toRVecFloat(tup.FatJet_phi),
        toRVecFloat(tup.FatJet_mass),
        toRVecFloat(tup.FatJet_rawFactor),
        toRVecFloat(tup.FatJet_area),
        toRVecFloat(tup.FatJet_msoftdrop),
        toRVecInt(tup.FatJet_subJetIdx1),
        toRVecInt(tup.FatJet_subJetIdx2),
        toRVecFloat(tup.SubJet_pt),
        toRVecFloat(tup.SubJet_eta),
        toRVecFloat(tup.SubJet_phi),
        toRVecFloat(tup.SubJet_mass),
        toRVecFloat(tup.SubJet_rawFactor),
        toRVecInt(tup.FatJet_jetId if addHEM2018Issue else [])
    ]
    rhoNames = ["fixedGridRhoFastjetAll", "Rho_fixedGridRhoFastjetAll"]
    for rhoName in rhoNames:
        try:
            rho = getattr(tup, rhoName)
            args.append(rho)
            break
        except AttributeError:
            pass
    else:
        raise RuntimeError(f"None of the attributes {rhoNames} found.")
    if isMC:
        args += [
            toRVecInt(tup.FatJet_genJetAK8Idx),
            tup.event,
            toRVecFloat(tup.GenJetAK8_pt),
            toRVecFloat(tup.GenJetAK8_eta),
            toRVecFloat(tup.GenJetAK8_phi),
            toRVecFloat(tup.GenJetAK8_mass)
        ]
    else:
        args += [toRVecInt([]), 0, toRVecFloat([]), toRVecFloat([]), toRVecFloat([]),
                 toRVecFloat([])]
    return args


def configureCalc(
        config, jecTag=None, jecLevel=None, smearingToolName=None, jsonFileSmearingTool=None,
        jerTag=None, splitJER=False, jesUncertaintySources=None, addHEM2018Issue=False,
        jsonFileSubjet=None, jetAlgoSubjet=None, jecTagSubjet=None, jecLevelSubjet=None):
    if jecTag:
        config.jecTag = jecTag
        config.jecLevel = jecLevel
        if jesUncertaintySources is not None:
            config.jesUncertainties = jesUncertaintySources
    if jerTag is not None:
        config.smearingToolName = smearingToolName
        config.jsonFileSmearingTool = jsonFileSmearingTool
        config.jerTag = jerTag
        config.splitJER = splitJER
        config.useGenMatch = True
    config.addHEM2018Issue = addHEM2018Issue
    if jsonFileSubjet is not None:
        config.jsonFileSubjet = jsonFileSubjet
        config.jetAlgoSubjet = jetAlgoSubjet
        config.jecTagSubjet = jecTagSubjet
        config.jecLevelSubjet = jecLevelSubjet


@pytest.fixture(scope="module")
def nanojetargsMC18():
    from bamboo.root import gbl
    f = gbl.TFile.Open(os.path.join(testData, "QCD_Run2018.root"))
    for tup in getEventWith(f, (lambda tup: tup.nJet >= 5)):
        yield getJetMETArgs(tup, isMC=True, forMET=False, PuppiMET=False)


@pytest.fixture(scope="module")
def nanojetargsMC18_postvalues():
    from bamboo.root import gbl
    f = gbl.TFile.Open(os.path.join(testData, "QCD_Run2018.root"))
    tup = f.Get("Events")
    res = []
    for i in range(tup.GetEntries()):
        tup.GetEntry(i)
        jet_vars = {"nominal": (toRVecFloat(tup.Jet_pt_nominal),
                                toRVecFloat(tup.Jet_mass_nominal))}
        from itertools import chain
        jet_vars.update(dict(chain.from_iterable(
            {f"jer{i:d}{vdir}": tuple(
                toRVecFloat(getattr(tup, f"Jet_{ivar}_jer{i:d}{vdir}"))
                for ivar in ("pt", "mass"))
                for vdir in ("up", "down")}.items() for i in range(6))))
        jet_vars.update(dict(chain.from_iterable(
            {f"jes{src}{vdir}": tuple(
                toRVecFloat(getattr(tup, f"Jet_{ivar}_jes{src}{vdir}".format(ivar, src)))
                for ivar in ("pt", "mass"))
                for vdir in ("up", "down")}.items()
            for src in ("Total", "FlavorQCD", "TimePtEta")
        )))
        res.append((getJetMETArgs(tup, isMC=True, forMET=False, PuppiMET=False), jet_vars))
    yield res


@pytest.fixture(scope="module")
def nanoMETargsMC18_postvalues():
    from bamboo.root import gbl
    f = gbl.TFile.Open(os.path.join(testData, "QCD_Run2018.root"))
    tup = f.Get("Events")
    res = []
    for i in range(tup.GetEntries()):
        tup.GetEntry(i)
        met_vars = {
            "nominal": (tup.MET_T1_pt_nominal, tup.MET_T1_phi_nominal),
            "unclustEnup": (tup.MET_T1_pt_unclustEnup, tup.MET_T1_phi_unclustEnup),
            "unclustEndown": (tup.MET_T1_pt_unclustEndown, tup.MET_T1_phi_unclustEndown)
        }
        met_vars.update(dict(
            ("{0}{1}".format(nm, var), (getattr(tup, "MET_T1_pt_{0}{1}".format(nm, var)),
                                        getattr(tup, "MET_T1_phi_{0}{1}".format(nm, var))))
            for var in ("up", "down") for nm in (
                ["jes{0}".format(jsnm) for jsnm in ("Total", "FlavorQCD", "TimePtEta")])
        ))
        res.append((tuple(getJetMETArgs(tup, isMC=True, forMET=True, PuppiMET=False)), met_vars))
    yield res


@pytest.fixture(scope="module")
def nanoSmearMETargsMC18_postvalues():
    from bamboo.root import gbl
    f = gbl.TFile.Open(os.path.join(testData, "QCD_Run2018.root"))
    tup = f.Get("Events")
    res = []
    for i in range(tup.GetEntries()):
        tup.GetEntry(i)
        met_vars = {
            "nominal": (tup.MET_T1Smear_pt_nominal, tup.MET_T1Smear_phi_nominal),
            "unclustEnup": (tup.MET_T1Smear_pt_unclustEnup, tup.MET_T1Smear_phi_unclustEnup),
            "unclustEndown": (tup.MET_T1Smear_pt_unclustEndown,
                              tup.MET_T1Smear_phi_unclustEndown)
        }
        met_vars.update(dict(
            ("{0}{1}".format(nm, var), (getattr(tup, "MET_T1Smear_pt_{0}{1}".format(nm, var)),
                                        getattr(tup, "MET_T1Smear_phi_{0}{1}".format(nm, var))))
            for var in ("up", "down") for nm in (
                [f"jer{i:d}" for i in range(6)]
                + ["jes{0}".format(jsnm) for jsnm in ("Total", "FlavorQCD", "TimePtEta")])
        ))
        res.append((tuple(getJetMETArgs(tup, isMC=True, forMET=True, PuppiMET=False)), met_vars))
    yield res


@pytest.fixture(scope="module")
def nanojetargsMC18_postvalues_hem():
    from bamboo.root import gbl
    f = gbl.TFile.Open(os.path.join(testData, "QCD_Run2018.root"))
    tup = f.Get("Events")
    res = []
    for i in range(tup.GetEntries()):
        tup.GetEntry(i)
        jet_vars = {
            "nominal": (toRVecFloat(tup.Jet_pt_nominal), toRVecFloat(tup.Jet_mass_nominal)),
        }
        from itertools import chain
        jet_vars.update(dict(chain.from_iterable(
            {f"{src}{vdir}": tuple(
                toRVecFloat(getattr(tup, f"Jet_{ivar}_{src}{vdir}".format(ivar, src)))
                for ivar in ("pt", "mass"))
                for vdir in ("up", "down")}.items() for src in (
                    [f"jer{i:d}" for i in range(6)]
                    + ["jes{0}".format(jsnm) for jsnm in ("Total", "FlavorQCD", "TimePtEta")])
        )))
        res.append((getJetMETArgs(tup, isMC=True, forMET=False, PuppiMET=False,
                                  addHEM2018Issue=True),
                    jet_vars))
    yield res


@pytest.fixture(scope="module")
def nanofatjetargsMC22_postvalues():
    from bamboo.root import gbl
    f = gbl.TFile.Open(os.path.join(testData, "QCD_Run2022.root"))
    tup = f.Get("Events")
    res = []
    for i in range(tup.GetEntries()):
        tup.GetEntry(i)
        jet_vars = {
            "nominal": (toRVecFloat(tup.FatJet_pt_nominal),
                        toRVecFloat(tup.FatJet_mass_nominal),
                        toRVecFloat(tup.FatJet_msoftdrop_nominal)),
        }
        from itertools import chain
        jet_vars.update(dict(chain.from_iterable(
            {f"jer{i:d}{vdir}": tuple(
                toRVecFloat(getattr(tup, f"FatJet_{ivar}_jer{i:d}{vdir}"))
                for ivar in ("pt", "mass", "msoftdrop"))
             for vdir in ("up", "down")}.items() for i in range(6))))
        jet_vars.update(dict(chain.from_iterable(
            {f"jes{src}{vdir}": tuple(
                toRVecFloat(getattr(tup, f"FatJet_{ivar}_jes{src}{vdir}".format(ivar, src)))
                for ivar in ("pt", "mass", "msoftdrop"))
             for vdir in ("up", "down")}.items() for src in ("Total", "FlavorQCD", "TimePtEta")
        )))
        res.append((getFatJetArgs(tup, isMC=True), jet_vars))
    yield res


@pytest.fixture(scope="module")
def nanofatjetargsMC22():
    from bamboo.root import gbl
    f = gbl.TFile.Open(os.path.join(testData, "QCD_Run2022.root"))
    for tup in getEventWith(f):
        yield getFatJetArgs(tup, isMC=True, addHEM2018Issue=False)


@pytest.fixture(scope="module")
def jetvarcalcMC18_smear():
    configCls = calcConfigs.JetVariations
    jsonFile = os.path.join(testData, "jet_jerc_2018UL.json.gz")
    jetType = "AK4PFchs"
    config = configCls(jsonFile, jetType)
    jsonFileSmearTool = os.path.join(testData, "jer_smear.json.gz")
    configureCalc(config, jecTag="Summer19UL18_V5_MC", jerTag="Summer19UL18_JRV2_MC",
                  jecLevel="L1L2L3Res", jsonFileSmearingTool=jsonFileSmearTool,
                  smearingToolName="JERSmear", splitJER=True)
    loadJMESystematicsCalculators()
    yield config.create()


@pytest.fixture(scope="module")
def jetvarcalcMC18_jec():
    configCls = calcConfigs.JetVariations
    jsonFile = os.path.join(testData, "jet_jerc_2018UL.json.gz")
    jetType = "AK4PFchs"
    config = configCls(jsonFile, jetType)
    jsonFileSmearTool = os.path.join(testData, "jer_smear.json.gz")
    configureCalc(config, jecTag="Summer19UL18_V5_MC", jerTag="Summer19UL18_JRV2_MC",
                  jecLevel="L1L2L3Res", jsonFileSmearingTool=jsonFileSmearTool,
                  smearingToolName="JERSmear", splitJER=True)
    loadJMESystematicsCalculators()
    yield config.create()


@pytest.fixture(scope="module")
def jetvarcalcMC18_jesunc():
    configCls = calcConfigs.JetVariations
    jsonFile = os.path.join(testData, "jet_jerc_2018UL.json.gz")
    jetType = "AK4PFchs"
    config = configCls(jsonFile, jetType)
    jsonFileSmearTool = os.path.join(testData, "jer_smear.json.gz")
    configureCalc(config, jecTag="Summer19UL18_V5_MC", jerTag="Summer19UL18_JRV2_MC",
                  jecLevel="L1L2L3Res", smearingToolName="JERSmear",
                  jsonFileSmearingTool=jsonFileSmearTool, splitJER=True,
                  jesUncertaintySources=["Total", "FlavorQCD", "TimePtEta"])
    loadJMESystematicsCalculators()
    yield config.create()


@pytest.fixture(scope="module")
def metvarcalcMC18_jesunc():
    configCls = calcConfigs.METVariations
    jsonFile = os.path.join(testData, "jet_jerc_2018UL.json.gz")
    jetType = "AK4PFchs"
    config = configCls(jsonFile, jetType)
    jsonFileSmearTool = os.path.join(testData, "jer_smear.json.gz")
    configureCalc(config, jecTag="Summer19UL18_V5_MC", jerTag="Summer19UL18_JRV2_MC",
                  jecLevel="L1L2L3Res", jsonFileSmearingTool=jsonFileSmearTool,
                  smearingToolName="JERSmear", splitJER=True,
                  jesUncertaintySources=["Total", "FlavorQCD", "TimePtEta"])
    loadJMESystematicsCalculators()
    yield config.create()


@pytest.fixture(scope="module")
def jetvarcalcMC18_hem():
    configCls = calcConfigs.JetVariations
    jsonFile = os.path.join(testData, "jet_jerc_2018UL.json.gz")
    jetType = "AK4PFchs"
    config = configCls(jsonFile, jetType)
    jsonFileSmearTool = os.path.join(testData, "jer_smear.json.gz")
    configureCalc(config, jecTag="Summer19UL18_V5_MC", jerTag="Summer19UL18_JRV2_MC",
                  jecLevel="L1L2L3Res", jsonFileSmearingTool=jsonFileSmearTool,
                  smearingToolName="JERSmear", splitJER=True, addHEM2018Issue=True,
                  jesUncertaintySources=["Total", "FlavorQCD", "TimePtEta"])
    loadJMESystematicsCalculators()
    yield config.create()


@pytest.fixture(scope="module")
def fatjetvarcalcMC22():
    configCls = calcConfigs.FatJetVariations
    jsonFile = os.path.join(testData, "fatjet_jerc_2022EE.json.gz")
    jetType = "AK8PFPuppi"
    config = configCls(jsonFile, jetType)
    jsonFileSmearTool = os.path.join(testData, "jer_smear.json.gz")
    configureCalc(config, jecTag="Summer22EEPrompt22_V1_MC", jerTag="Summer22EEPrompt22_JRV1_MC",
                  jecLevel="L1L2L3Res", jesUncertaintySources=["Total", "FlavorQCD", "TimePtEta"],
                  jsonFileSmearingTool=jsonFileSmearTool, smearingToolName="JERSmear",
                  splitJER=True, addHEM2018Issue=True, jetAlgoSubjet="AK4PFPuppi",
                  jsonFileSubjet=os.path.join(testData, "jet_jerc_2022EE.json.gz"),
                  jecTagSubjet="Summer22EEPrompt22_V1_MC", jecLevelSubjet="L1L2L3Res")
    loadJMESystematicsCalculators()
    yield config.create()


def test_jetvarcalcMC18_smear(jetvarcalcMC18_smear):
    assert jetvarcalcMC18_smear


def test_jetvarcalcMC18_nano_smear(jetvarcalcMC18_smear, nanojetargsMC18):
    res = jetvarcalcMC18_smear.produce(*nanojetargsMC18)
    assert res


def test_jetvarcalcMC18_nano_jec(jetvarcalcMC18_jec, nanojetargsMC18):
    res = jetvarcalcMC18_jec.produce(*nanojetargsMC18)
    assert res


def test_jetvarcalcMC18_nano_jesunc(jetvarcalcMC18_jesunc, nanojetargsMC18):
    res = jetvarcalcMC18_jesunc.produce(*nanojetargsMC18)
    assert res


def test_fatjetvarcalcMC22_nano(fatjetvarcalcMC22, nanofatjetargsMC22):
    res = fatjetvarcalcMC22.produce(*nanofatjetargsMC22)
    assert res


def isclose_float(a, b, tol=1.):
    from bamboo.root import gbl
    return math.isclose(a, b, rel_tol=tol * gbl.std.numeric_limits["float"].epsilon())


def compareJets(names, calcRes, postValues, tol=1.):
    hasDiff = False
    for ky, (post_pt, post_mass) in postValues.items():
        idx = names.index(ky)
        print(ky, "pt", calcRes.pt(idx), post_pt)
        print(ky, "m ", calcRes.mass(idx), post_mass)
        if not (all(isclose_float(a, b, tol=tol)
                    for a, b in zip(post_pt, calcRes.pt(idx)))
                and all(isclose_float(a, b, tol=tol)
                        for a, b in zip(post_mass, calcRes.mass(idx)))):
            print(f"FAIL: Difference for {ky}")
            hasDiff = True
    return not hasDiff


def compareFatJets(namesAll, namesM, calcRes, postValues, tol=1.):
    hasDiff = False
    for ky, post in postValues.items():
        if ky in namesAll:
            idx = namesAll.index(ky)
            (post_pt, post_mass, post_msd) = post
            print(ky, "pt ", calcRes.pt(idx), post_pt)
            print(ky, "m  ", calcRes.mass(idx), post_mass)
            print(ky, "msd", calcRes.msoftdrop(idx), post_msd)
            eq_pt = all(isclose_float(a, b, tol=tol)
                        for a, b in zip(post_pt, calcRes.pt(idx)))
            eq_m = all(isclose_float(a, b, tol=tol)
                       for a, b in zip(post_mass, calcRes.mass(idx)))
            eq_msd = all(isclose_float(a, b, tol=tol)
                         for a, b in zip(post_msd, calcRes.msoftdrop(idx)))
            if not (eq_pt and eq_m and eq_msd):
                what = ", ".join((["pt"] if not eq_pt else [])
                                 + (["mass"] if not eq_m else [])
                                 + (["msd"] if not eq_msd else []))
                print(f"FAIL: Difference for {ky} (in {what})")
                hasDiff = True
        else:
            idx = len(namesAll) + namesM.index(ky)
            (post_mass, post_msd) = post
            print(ky, "m  ", calcRes.mass(idx), post_mass)
            print(ky, "msd", calcRes.msoftdrop(idx), post_msd)
            eq_m = all(isclose_float(a, b, tol=tol)
                       for a, b in zip(post_mass, calcRes.mass(idx)))
            eq_msd = all(isclose_float(a, b, tol=tol)
                         for a, b in zip(post_msd, calcRes.msoftdrop(idx)))
            if not (eq_m and eq_msd):
                what = ", ".join((["mass"] if not eq_m else [])
                                 + (["msd"] if not eq_msd else []))
                print(f"FAIL: Difference for {ky} (in {what})")
                hasDiff = True
    return not hasDiff


def compareMET(names, calcRes, postValues, reltol_pt=1.e-6, reltol_phi=1.e-6, abstol_phi=1.e-6):
    hasDiff = False
    for ky, (post_pt, post_phi) in postValues.items():
        idx = names.index(ky)
        print(ky, "pt ", calcRes.pt(idx), post_pt)
        print(ky, "phi", calcRes.phi(idx), post_phi)
        if not (math.isclose(calcRes.pt(idx), post_pt, rel_tol=1.e-6)
                and math.isclose(calcRes.phi(idx), post_phi, rel_tol=1.e-6, abs_tol=1.e-6)):
            print(f"FAIL: Difference for {ky}")
            hasDiff = True
    return not hasDiff


def test_jetvarcalc_nanopost_jesunc(jetvarcalcMC18_jesunc, nanojetargsMC18_postvalues):
    for nanojetargsMC18, postValues in nanojetargsMC18_postvalues:
        assert compareJets([str(nm) for nm in jetvarcalcMC18_jesunc.available()],
                           jetvarcalcMC18_jesunc.produce(*nanojetargsMC18),
                           postValues)


def test_metvarcalc_nanopost_jesunc(metvarcalcMC18_jesunc, nanoMETargsMC18_postvalues):
    metvarcalcMC18_jesunc.setIsT1SmearedMET(False)
    for nanoMETargsMC18, postValues in nanoMETargsMC18_postvalues:
        assert compareMET([str(nm) for nm in metvarcalcMC18_jesunc.available()],
                          metvarcalcMC18_jesunc.produce(*nanoMETargsMC18),
                          postValues)


def test_metvarcalc_nanopost_jesunc_T1Smear(
        metvarcalcMC18_jesunc,
        nanoSmearMETargsMC18_postvalues):
    metvarcalcMC18_jesunc.setIsT1SmearedMET(True)
    for nanoMETargsMC18, postValues in nanoSmearMETargsMC18_postvalues:
        assert compareMET([str(nm) for nm in metvarcalcMC18_jesunc.available()],
                          metvarcalcMC18_jesunc.produce(*nanoMETargsMC18), postValues)


def test_jetvarcalc_nanopost_jesunc_HEM(jetvarcalcMC18_hem, nanojetargsMC18_postvalues_hem):
    for nanojetargsMC18, postValues in nanojetargsMC18_postvalues_hem:
        assert compareJets([str(nm) for nm in jetvarcalcMC18_hem.available()],
                           jetvarcalcMC18_hem.produce(*nanojetargsMC18),
                           postValues, tol=2.)


def test_fatjetvarcalc_nanopost_jesunc(fatjetvarcalcMC22, nanofatjetargsMC22_postvalues):
    for fatjetargs, postValues in nanofatjetargsMC22_postvalues:
        avlAll = [str(nm) for nm in fatjetvarcalcMC22.available()]
        avlM = [str(nm) for nm in fatjetvarcalcMC22.available("mass") if nm not in avlAll]
        assert compareFatJets(avlAll, avlM,
                              fatjetvarcalcMC22.produce(*fatjetargs),
                              postValues)
