"""
General (not bamboo-specific) utilities
"""
import os
import os.path
import subprocess
import math
from . import treefunctions as tf


# based on https://stackoverflow.com/a/64474435/11079521
def getlcwd():
    """ os.getcwd() with a logical path ($PWD, if available, or `pwd -L`) """
    if "PWD" in os.environ:
        return os.environ["PWD"]
    else:
        return subprocess.check_output(["pwd", "-L"]).decode().strip()


def labspath(path):
    """ os.path.abspath using pwd with symbolic links """
    if os.path.isabs(path):
        return path
    else:
        return os.path.join(getlcwd(), path)


class _DirsAboveStat:
    def __init__(self, aPath):
        self.aPath = aPath
        self.above = {}
        iP = aPath
        while os.path.dirname(iP) != iP:
            self.above[iP] = None
            iP = os.path.dirname(iP)

    def __iter__(self):
        yield from self.above.keys()

    def __getitem__(self, dPath):
        if dPath not in self.above:
            raise KeyError(f"{dPath} not found")
        if self.above[dPath] is None:
            self.above[dPath] = os.stat(dPath)
        return self.above[dPath]


def realcommonpath(pathA, pathB):
    """ Use os.path.samefile to find the lowest common base dir """
    dirsA = _DirsAboveStat(pathA)
    dirsB = _DirsAboveStat(pathB)
    for dA in dirsA:
        for dB in dirsB:
            if dirsA[dA] == dirsB[dB]:
                return dA, dB


def photonEtaSC(photon, PV):
    """ SuperCluster eta for photon """
    SCEta = tf.multiSwitch(
        (photon.isScEtaEB, photonEtaSCEB(photon, PV)),
        (photon.isScEtaEE, photonEtaSCEE(photon, PV)),
        photon.eta)

    return SCEta


def photonEtaSCEE(photon, PV):
    """ SuperCluster eta for photon in the endcap """
    tgThetaOver2 = tf.exp(-photon.eta)
    tgTheta = 2.0 * tgThetaOver2 / (1.0 - tgThetaOver2 * tgThetaOver2)
    tgScTheta = 0.0
    intersectionZ = tf.switch(photon.eta > 0.0, 310.0, -310.0)
    base = tf.switch(photon.eta > 0.0, 310.0 - PV.z, -310.0 - PV.z)
    r = base * tgTheta
    crystalX = PV.x + r * tf.cos(photon.phi)
    crystalY = PV.y + r * tf.sin(photon.phi)
    tgScTheta = tf.sqrt(crystalX * crystalX + crystalY * crystalY) / intersectionZ
    scTheta = tf.switch(tf.atan(tgScTheta) < 0.0, tf.atan(tgScTheta) + math.pi, tf.atan(tgScTheta))
    tgScThetaOver2 = tf.tan(scTheta / 2.0)

    return -tf.log(tgScThetaOver2)


def photonEtaSCEB(photon, PV):
    """ SuperCluster eta for photon in the barrel """
    tgThetaOver2 = tf.exp(-photon.eta)
    tgTheta = 2.0 * tgThetaOver2 / (1.0 - tgThetaOver2 * tgThetaOver2)
    tgScTheta = 0.0
    R = 130.0

    angleX0Y0 = tf.multiSwitch(
        (PV.x > 0.0, tf.atan(PV.y / PV.x)),
        (PV.x < 0.0, math.pi + tf.atan(PV.y / PV.x)),
        (PV.y > 0.0, math.pi / 2.0),
        -math.pi / 2.0)

    alpha = angleX0Y0 + (math.pi - photon.phi)
    sinBeta = tf.sqrt(PV.x * PV.x + PV.y * PV.y) / R * tf.sin(alpha)
    beta = tf.abs(tf.asin(sinBeta))
    gamma = math.pi / 2.0 - alpha - beta
    rho = PV.x * PV.x + PV.y * PV.y
    z0ZSC = tf.sqrt(R * R + rho - 2.0 * R * tf.sqrt(rho) * tf.cos(gamma)) / tgTheta
    tgScTheta = R / (PV.z + z0ZSC)
    scTheta = tf.switch(tf.atan(tgScTheta) < 0.0, tf.atan(tgScTheta) + math.pi, tf.atan(tgScTheta))
    tgScThetaOver2 = tf.tan(scTheta / 2.0)

    return -tf.log(tgScThetaOver2)
