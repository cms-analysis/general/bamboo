"""
Example analysis module: make a skimmed ttree with jets and met from a NanoAOD
"""
import logging
import os.path
import re

from bamboo.analysismodules import NanoAODModule, NanoAODSkimmerModule

logger = logging.getLogger(__name__)


JECTagDatabase = {
    "2018UL": {
        "MC": "Summer19UL18_V5_MC",
        "D": "Summer19UL18_RunD_V5_DATA"},
    "2022EE": {
        "MC": "Summer22EEPrompt22_V1_MC",
        "F": "Summer22EEPrompt22_RunF_V1_DATA"},
}

JERTagDatabase = {
    "2018UL": "Summer19UL18_JRV2_MC",
    "2022EE": "Summer22EEPrompt22_JRV1_MC",
}

JSONFiles = {
    "2018UL": {
        "AK4": "jet_jerc_2018UL.json.gz",
        "AK8": "fatjet_jerc_2018UL.json.gz"},
    "2022EE": {
        "AK4": "jet_jerc_2022EE.json.gz",
        "AK8": "fatjet_jerc_2022EE.json.gz"},
}


def getRunEra(sample):
    """Return run era (A/B/...) for data sample"""
    result = re.search(r'Run20..([A-Z]?)', sample)
    print(sample)
    if result is None:
        return "MC"
    else:
        return result.group(1)


class NanoQCDJetsBase(NanoAODModule):
    """ Base module for NanoAOD Jets, MET, Fatjets example """
    def addArgs(self, parser):
        super().addArgs(parser)
        parser.add_argument("--backend", type=str, default="dataframe",
                            help="Backend to use, 'dataframe' (default), 'lazy', or 'compiled'")
        parser.add_argument("--postprocessed", action="store_true",
                            help="Run on postprocessed NanoAOD")

    def prepareTree(self, tree, sample=None, sampleCfg=None, backend=None):
        if self.args.postprocessed:
            return self.prepare_postprocessed(tree, sample=sample, sampleCfg=sampleCfg, backend=backend)
        else:
            return self.prepare_ondemand(tree, sample=sample, sampleCfg=sampleCfg, backend=backend)

    def prepare_ondemand(self, tree, sample=None, sampleCfg=None, backend=None):
        era = sampleCfg.get("era") if sampleCfg else None
        isMC = self.isMC(sample)
        metName = "MET" if era == "2018UL" else "PuppiMET"
        # Decorate the tree
        from bamboo.treedecorators import NanoAODDescription, nanoFatJetCalc, CalcCollectionsGroups
        nanoJetMETCalc_both = CalcCollectionsGroups(
            Jet=("pt", "mass"), changes={metName: (f"{metName}T1", f"{metName}T1Smear")},
            **{metName: ("pt", "phi")})
        nanoJetMETCalc_data = CalcCollectionsGroups(
            Jet=("pt", "mass"), changes={metName: (f"{metName}T1",)},
            **{metName: ("pt", "phi")})

        systVars = (([nanoFatJetCalc] if era == "2022EE" else [])
                    + [nanoJetMETCalc_both if isMC else nanoJetMETCalc_data])
        tree, noSel, be, lumiArgs = super().prepareTree(
            tree, sample=sample, sampleCfg=sampleCfg,
            description=NanoAODDescription.get(
                "v9" if era == "2018UL" else "v12", year=era[:4], isMC=isMC, systVariations=systVars),
            backend=self.args.backend or backend)
        # always-on event weights
        if isMC:
            mcWgts = [tree.genWeight]
            noSel = noSel.refine("mcWeight", weight=mcWgts)
        runEra = getRunEra(sample)
        jecTag = JECTagDatabase[era]["MC" if isMC else runEra]
        smearTag = JERTagDatabase[era] if isMC else None
        cmJMEArgs = {
            "jsonFile": os.path.join(os.path.dirname(os.path.dirname(__file__)),
                                     "tests", "data", JSONFiles[era]["AK4"]),
            "jec": jecTag,
            "smear": smearTag,
            "jsonFileSmearingTool": os.path.join(os.path.dirname(os.path.dirname(__file__)),
                                                 "tests", "data", "jer_smear.json.gz"),
            "splitJER": True,
            "jesUncertaintySources": (["Total", "FlavorQCD", "TimePtEta"] if isMC else None),
            "isMC": isMC,
            "addHEM2018Issue": True if era == "2018UL" else False,
            "backend": be,
        }
        # configure corrections and variations
        from bamboo.analysisutils import configureJets, configureType1MET
        configureJets(tree._Jet, jetType="AK4PFchs" if era == "2018UL" else "AK4PFPuppi", **cmJMEArgs)
        if isMC:
            configureType1MET(getattr(tree, f"_{metName}T1Smear"), isT1Smear=True, **cmJMEArgs)
            print("done")
        configureType1MET(
            getattr(tree, f"_{metName}T1"),
            enableSystematics=((lambda v: not v.startswith("jer")) if isMC else None),
            **cmJMEArgs)
        if era == "2022EE":
            cmJMEArgs.update({"jsonFile": os.path.join(os.path.dirname(os.path.dirname(__file__)),
                                                       "tests", "data", JSONFiles[era]["AK8"]), })
            cmJMEArgs.update({"jetAlgoSubjet": "AK4PFPuppi", })
            cmJMEArgs.update({"jecSubjet": jecTag, })
            cmJMEArgs.update({"jsonFileSubjet": os.path.join(os.path.dirname(os.path.dirname(__file__)),
                                                             "tests", "data", JSONFiles[era]["AK4"]), })
            configureJets(tree._FatJet, jetType="AK8PFPuppi", **cmJMEArgs)

        return tree, noSel, be, lumiArgs


class SkimNanoQCDJets(NanoQCDJetsBase, NanoAODSkimmerModule):
    def defineSkimSelection(self, tree, noSel, sample=None, sampleCfg=None):
        from bamboo import treefunctions as op
        era = sampleCfg.get("era") if sampleCfg else None
        isMC = self.isMC(sample)
        metName = "MET" if era == "2018UL" else "PuppiMET"
        # Jets
        varsToKeep = {"nJet": None}
        for varNm in ("pt", "eta", "phi", "mass", "rawFactor", "area",
                      "muonSubtrFactor", "neEmEF", "chEmEF", "jetId"):
            varsToKeep.update({f"Jet_{varNm}": None})
        # Low Pt jets for MET
        varsToKeep.update({"nCorrT1METJet": None})
        for varNm in ("rawPt", "eta", "phi", "area", "muonSubtrFactor"):
            varsToKeep.update({f"CorrT1METJet_{varNm}": None})
        # MET
        varsToKeep.update({f"Raw{metName}_phi": None, f"Raw{metName}_pt": None})
        varsToKeep.update({f"{metName}_phi": None, f"{metName}_pt": None})
        if era == "2018UL":
            varsToKeep.update({"fixedGridRhoFastjetAll": None})
            varsToKeep.update({"MET_MetUnclustEnUpDeltaX": None})
            varsToKeep.update({"MET_MetUnclustEnUpDeltaY": None})
        if era == "2022EE":
            varsToKeep.update({"Rho_fixedGridRhoFastjetAll": None})
            varsToKeep.update({"PuppiMET_phiUnclusteredUp": None})
            varsToKeep.update({"PuppiMET_ptUnclusteredUp": None})
            # Fatjets
            varsToKeep.update({"nFatJet": None})
            for varNm in ("pt", "eta", "phi", "mass", "rawFactor",
                          "area", "msoftdrop", "subJetIdx1", "subJetIdx2"):
                varsToKeep.update({f"FatJet_{varNm}": None})
            # Subjets
            varsToKeep.update({"nSubJet": None})
            for varNm in ("pt", "eta", "phi", "mass", "rawFactor"):
                varsToKeep.update({f"SubJet_{varNm}": None})
        if isMC:
            # Gen jets
            varsToKeep.update({"Jet_genJetIdx": None, "Jet_partonFlavour": None})
            varsToKeep.update({"run": None, "luminosityBlock": None, "event": None})
            varsToKeep.update({"nGenJet": None})
            for varNm in ("pt", "eta", "phi", "mass"):
                varsToKeep.update({f"GenJet_{varNm}": None})
            if era == "2022EE":
                varsToKeep.update({"FatJet_genJetAK8Idx": None, "nGenJetAK8": None})
                varsToKeep.update({"nSubGenJetAK8": None})
                for varNm in ("pt", "eta", "phi", "mass"):
                    varsToKeep.update({f"GenJetAK8_{varNm}": None})
                    varsToKeep.update({f"SubGenJetAK8_{varNm}": None})
        # corrected jet, fatjet, MET
        jets = tree._Jet["nominal"]
        varsToKeep["Jet_pt_nominal"] = op.map(jets, lambda j: j.pt)
        varsToKeep["Jet_mass_nominal"] = op.map(jets, lambda j: j.mass)
        met = getattr(tree, f"_{metName}T1")["nominal"]
        varsToKeep[f"{metName}_T1_pt_nominal"] = met.pt
        varsToKeep[f"{metName}_T1_phi_nominal"] = met.phi
        if era == "2022EE":
            fatjets = tree._FatJet["nominal"]
            varsToKeep["FatJet_pt_nominal"] = op.map(fatjets, lambda fj: fj.pt)
            varsToKeep["FatJet_mass_nominal"] = op.map(fatjets, lambda fj: fj.mass)
            varsToKeep["FatJet_msoftdrop_nominal"] = op.map(fatjets, lambda fj: fj.msoftdrop)
        if isMC:
            met = getattr(tree, f"_{metName}T1Smear")["nominal"]
            varsToKeep[f"{metName}_T1Smear_pt_nominal"] = met.pt
            varsToKeep[f"{metName}_T1Smear_phi_nominal"] = met.phi
            sysNms = ["jesTotal", "jesFlavorQCD", "jesTimePtEta"] + [f"jer{i}" for i in range(6)]
            for sysNm in sysNms:
                for var in ("up", "down"):
                    jets = tree._Jet[f"{sysNm}{var}"]
                    varsToKeep[f"Jet_pt_{sysNm}{var}"] = op.map(jets, lambda j: j.pt)
                    varsToKeep[f"Jet_mass_{sysNm}{var}"] = op.map(jets, lambda j: j.mass)
                    for smear in ("", "Smear"):
                        if varNm in [f"jer{i}" for i in range(6)] and smear == "":
                            continue
                        else:
                            met = getattr(tree, f"_{metName}T1{smear}")[f"{sysNm}{var}"]
                            varsToKeep[f"{metName}_T1{smear}_pt_{sysNm}{var}"] = met.pt
                            varsToKeep[f"{metName}_T1{smear}_phi_{sysNm}{var}"] = met.phi
                    if era == "2022EE":
                        fatjets = tree._FatJet[f"{sysNm}{var}"]
                        varsToKeep[f"FatJet_pt_{sysNm}{var}"] = op.map(fatjets, lambda fj: fj.pt)
                        varsToKeep[f"FatJet_mass_{sysNm}{var}"] = op.map(fatjets, lambda fj: fj.mass)
                        fj_msd = f"FatJet_msoftdrop_{sysNm}{var}"
                        varsToKeep[fj_msd] = op.map(fatjets, lambda fj: fj.msoftdrop)
            for var in ("up", "down"):
                for smear in ("", "Smear"):
                    met = getattr(tree, f"_{metName}T1{smear}")[f"unclustEn{var}"]
                    varsToKeep[f"{metName}_T1{smear}_pt_unclustEn{var}"] = met.pt
                    varsToKeep[f"{metName}_T1{smear}_phi_unclustEn{var}"] = met.phi
            if era == "2018UL":
                for var in ("up", "down"):
                    jets = tree._Jet[f"jesHEMIssue{var}"]
                    varsToKeep[f"Jet_pt_jesHEMIssue{var}"] = op.map(jets, lambda j: j.pt)
                    varsToKeep[f"Jet_mass_jesHEMIssue{var}"] = op.map(jets, lambda j: j.mass)
                    for smear in ("", "Smear"):
                        met = getattr(tree, f"_{metName}T1{smear}")[f"jesHEMIssue{var}"]
                        varsToKeep[f"{metName}_T1{smear}_pt_jesHEMIssue{var}"] = met.pt
                        varsToKeep[f"{metName}_T1{smear}_phi_jesHEMIssue{var}"] = met.phi
        return noSel, varsToKeep
